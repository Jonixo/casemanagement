package com.databoss.furkan.CaseManagement;


import java.util.HashMap;
import java.util.List;

import com.databoss.furkan.Classes.Case;
import com.databoss.furkan.Classes.CaseShare;
import com.databoss.furkan.Service.ICaseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class CaseManagementController {
    @Autowired
    private ICaseService caseService;

    @ApiOperation(value= "Get all cases.", response = Case.class)
    @GetMapping("/case")
    public List getAllCases() {

        return  caseService.getAllCases();
    }

    @ApiOperation(value= "Create case.  {\"workId\":testworkids[],\"permissions\":\"testuser,testpermissionlevel\"}", response = Case.class)
    @PostMapping("/case")
    public Case createCase(@Valid @RequestBody Case case_) {

        return  caseService.createCase(case_);
    }

    @ApiOperation(value= "Get case by Id. Get Request => /api/case/{id}", response = Case.class)
    @GetMapping("/case/{id}")
    public Case loadCase(@PathVariable("id") Long caseId) {

        return caseService.loadCase(caseId);
    }

    @ApiOperation(value= "Update case by Id. Put Request => /api/case/{id}, {\"workId\":testworkids[],\"permissions\":\"testuser,testpermissionlevel\"} ", response = Case.class)
    @PutMapping("/case/{id}")
    public Case updateCase( @PathVariable ("id") Long caseId, @RequestBody Case case_ ) {

        return caseService.updateCase(caseId,case_);
    }

    @ApiOperation(value= "Delete case by Id. Delete Request => /api/case/{id}", response = Case.class)
    @DeleteMapping("/case/{caseid}")
    public ResponseEntity<Void> deleteCase(@PathVariable("caseid") Long caseId) {

        return caseService.deleteCase(caseId);
    }

    @ApiOperation(value= "Get permission of a case by Id. Get Request => /api/casePermissions/{caseid}", response = Case.class)
    @GetMapping("/case/permissions/{caseid}")
    public HashMap<String,Integer> getpermissions(@PathVariable("caseid") Long caseId){

        return caseService.getPermissions(caseId);
    }

    @ApiOperation(value= "Give permission to another user in the given case id. Put Request => /api/share , {\"userName\":\"testuser\",\"permissionLevel\":testpermissionlevel} ", response = Case.class)
    @PutMapping("/case/share")
    public Case shareCase(@RequestBody CaseShare shareInfo) {

        return caseService.shareCase(shareInfo);
    }

    @ApiOperation(value= "Add a work to an existing case. Put Request => /api/case/{caseid}/{workid} ", response = Case.class)
    @PutMapping("/case/{caseid}/{workid}")
    public Case addWorkToCase(@PathVariable("caseid")  Long caseId,@PathVariable("workid")  Long workId) {

        return caseService.addWorkToCase(caseId, workId);
    }

    @ApiOperation(value= "Delete a work to an existing case. Delete Request => /api/case/{caseid}/{workid} ", response = Case.class)
    @DeleteMapping("/case/{caseid}/{workid}")
    public Case deleteWorkFromCase(@PathVariable("caseid") Long caseId,@PathVariable("workid") Long workId) {

        return caseService.deleteWorkFromCase(caseId, workId);
    }
}
