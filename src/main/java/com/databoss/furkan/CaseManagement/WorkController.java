package com.databoss.furkan.CaseManagement;

import com.databoss.furkan.Classes.Work;
import com.databoss.furkan.Service.IWorkService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class WorkController {

        @Autowired
        private IWorkService workService;

        @ApiOperation(value= "Get work by Id. Get Request => /api/work/{id}", response = Work.class)
        @GetMapping("/work/{id}")
        public Work loadWork(@PathVariable("id") Long workId) {

            return workService.loadWork(workId);

        }

        @ApiOperation(value= "Create work. {\"caseId\":testcaseid,\"data\":\"testdata\"} ", response = Work.class)
        @PostMapping("/work")
        public Work createWork (@Valid @RequestBody Work work) {

                return workService.createWork(work);
        }

        @ApiOperation(value= "Update work by id,  Put Request => /api/work/{id}, {\"caseId\":testcaseid,\"data\":\"testdata\"} ", response = Work.class)
        @PutMapping("/work/{id}")
        public Work updateWork(@PathVariable("id") Long workId,@RequestBody Work work) {

            return workService.updateWork(workId,work);

        }

        @ApiOperation(value= "Delete work by id,  Delete Request => /api/work/{id},", response = Work.class)
        @DeleteMapping("/work/{id}")
        public ResponseEntity<Void> deleteWork(@PathVariable("id") Long workId) {
            return workService.deleteWork(workId);
        }

}
