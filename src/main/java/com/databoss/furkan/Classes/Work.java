package com.databoss.furkan.Classes;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="Works")
@EntityListeners(AuditingEntityListener.class)
public class Work implements Serializable {

    @Id
    @Column(name="WorkId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long workId;

    @NotNull
    @Column(name="CaseId")
    private Long caseId;

    @NotNull
    @Column(name="Data")
    private String data;

    public Work(Long workId,@NotNull Long caseId,@NotNull String data) {
        this.workId = workId;
        this.caseId = caseId;
        this.data = data;
    }

    public Work() {
        //For Spring Boot
    }

    public Long getWorkId() {
        return workId;
    }

    public void setWorkId(Long workId) {
        this.workId = workId;
    }

    public Long getCaseId() {
        return caseId;
    }

    public void setCase_id(Long case_id) {
        this.caseId = case_id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


}
