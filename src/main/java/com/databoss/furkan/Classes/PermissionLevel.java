package com.databoss.furkan.Classes;


import java.io.Serializable;

public class PermissionLevel implements Serializable {
    private static final int READ=1;
    private static final int WRITE=2;
    private static final int DELETE=3;
}
