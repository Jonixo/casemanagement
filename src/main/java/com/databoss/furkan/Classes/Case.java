package com.databoss.furkan.Classes;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;


@Entity
@Table(name="Cases")
@EntityListeners(AuditingEntityListener.class)
public class Case implements Serializable {

    @Id
    @Column(name="CaseId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long caseId;

    @ElementCollection
    @Column(name="WorkIds")
    private Set<Long> workIds;

    @NotNull
    @Column(name="Permission")
    private String permissions;


    public Case() {
        //For Spring Boot
    }

    public Case(Long caseId,Set<Long> workId,@NotNull String permissions){
        this.caseId=caseId;
        this.workIds=workId;
        this.permissions=permissions;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public Long getCaseId() {
        return caseId;
    }

    public Set<Long> getWorkId() {
        return workIds;
    }

    public void setWorkId(Set<Long> workId) {
        this.workIds = workId;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
}
