package com.databoss.furkan.Service;

import com.databoss.furkan.Classes.Case;
import com.databoss.furkan.Classes.CaseShare;
import com.databoss.furkan.Dao.ICaseDAO;
import com.databoss.furkan.Dao.IWorkDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class CaseService implements ICaseService {

    @Autowired
    private ICaseDAO caseDAO;

    @Autowired
    private IWorkDAO workDAO;

    @Override
    public Case loadCase(Long caseId) {
        Case case_ =caseDAO.loadCase(caseId);
        //If there is no case, method cant return it
        if(case_==null)
            return null;
        else return case_;

    }

    @Override
    public List<Case> getAllCases(){
        return caseDAO.getAllCases();
    }

    @Override
    public synchronized Case createCase(Case case_){

      return caseDAO.createCase(case_);
    }

    @Override
    public Case updateCase(Long caseId,Case case_) {
        //If there is no case, method cant return it
        if(loadCase(caseId)==null)
            return null;
        return caseDAO.updateCase(case_,caseId);
    }

    @Override
    public ResponseEntity<Void> deleteCase(Long caseId) {
        //If there is no case, method cant delete it
        if(loadCase(caseId)==null)
            return null;
        else
        caseDAO.deleteCase(caseId);
        return ResponseEntity.ok().build();
    }

    //Work with Ege
    @Override
    public Case shareCase(CaseShare shareInfo){
        //If there is no case, method cant return it
        if(loadCase(shareInfo.getCaseId())==null)
            return null;
        return caseDAO.shareCase(shareInfo.getCaseId(),shareInfo.getUserName(),shareInfo.getPermissionLevel());
    }

    @Override
    public Case addWorkToCase(Long caseId,Long workId){
        //If there is no case, method cant return it
        if(loadCase(caseId)==null)
            return null;
        return caseDAO.addWorkToCase(caseId,workId);
    }

    @Override
    public Case deleteWorkFromCase(Long caseId,Long workId){
        //If there is no case, method cant return it
        if(loadCase(caseId)==null)
            return null;
        return caseDAO.deleteWorkFromCase(caseId,workId);
    }

    @Override
    public HashMap<String,Integer> getPermissions(Long caseId){
        //If there is no case, method cant return it
        if(loadCase(caseId)==null)
            return null;
        HashMap<String,Integer> permissionHashmap= new HashMap<>();
      String[] permissions=loadCase(caseId).getPermissions().split(",");
      for(int i=0;i<permissions.length-1;i+=2)
          permissionHashmap.put(permissions[i],Integer.parseInt(permissions[i+1]));
      return permissionHashmap;
    }

}
