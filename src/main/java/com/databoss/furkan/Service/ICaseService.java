package com.databoss.furkan.Service;

import com.databoss.furkan.Classes.Case;
import com.databoss.furkan.Classes.CaseShare;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface ICaseService {

    List<Case> getAllCases();

    Case loadCase(Long caseId);

    Case createCase(Case case_);

    Case updateCase(Long caseId,Case case_);

    ResponseEntity<Void> deleteCase(Long caseId);

    Case shareCase(CaseShare shareinfo);

    Case addWorkToCase(Long caseId,Long workId);

    Case deleteWorkFromCase(Long caseId,Long workId);

    HashMap<String,Integer> getPermissions(Long caseId);


}
