package com.databoss.furkan.Service;

import com.databoss.furkan.Classes.Work;
import org.springframework.http.ResponseEntity;

public interface IWorkService {

    Work loadWork(Long workId);

    Work createWork(Work work);

    Work updateWork(Long workId,Work work);

    ResponseEntity<Void> deleteWork(Long workId);

}
