package com.databoss.furkan.Service;

import com.databoss.furkan.Classes.Work;
import com.databoss.furkan.Dao.IWorkDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class WorkService implements IWorkService {

    @Autowired
    private IWorkDAO workDAO;

    @Autowired
    private ICaseService caseService;

    @Override
    public Work loadWork(Long workId) {
        Work work =workDAO.loadWork(workId);
        //If there is no work method cant return work
        if(work==null)
            return null;
        else return work;
    }

    @Override
    public synchronized Work createWork(Work work){
        //if case (given in work) is not existing user cant create work
        if(caseService.loadCase(work.getCaseId())==null)
            return null;

        return workDAO.createWork(work);
    }

    @Override
    public Work updateWork(Long workId,Work work) {
        //If there is no work method cant return work
        if (loadWork(workId)==null)
            return null;
        return workDAO.updateWork(work,workId);
    }

    @Override
    public ResponseEntity<Void> deleteWork(Long workId) {
        //If there is no work method cant delete work
        if(loadWork(workId)==null)
            return null;
        workDAO.deleteWork(workId);
        return ResponseEntity.ok().build();
    }

}
