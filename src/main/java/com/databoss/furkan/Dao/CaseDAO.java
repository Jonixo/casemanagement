package com.databoss.furkan.Dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.databoss.furkan.Classes.Case;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class CaseDAO implements ICaseDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Case loadCase(Long CaseId) {
        return entityManager.find(Case.class, CaseId);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Case> getAllCases() {
        return (List<Case>) entityManager.createQuery("from Case").getResultList();
    }

    @Override
    public Case createCase(Case case_) {
        entityManager.persist(case_);
        entityManager.flush();
        return loadCase(case_.getCaseId());
    }

    @Override
    public Case updateCase(Case case_,Long caseId) {
        Case case_up = loadCase(caseId);
        case_up.setWorkId(case_.getWorkId());
        case_up.setPermissions(case_.getPermissions());
        entityManager.flush();
        return loadCase(caseId);
    }

    @Override
    public void deleteCase(Long caseId) {
        entityManager.remove(loadCase(caseId));
        entityManager.flush();
    }


    //Will work with ege
    @Override
    public Case shareCase(Long caseId, String userName, int permissionLevel){
        Case case_=loadCase(caseId);
        String[] permissionLevelsharer=case_.getPermissions().split(",");
         if(Integer.parseInt(permissionLevelsharer[1])<=permissionLevel) {
             case_.setPermissions(case_.getPermissions()+","+userName+","+permissionLevel+",");
             return case_;
         }
         return null;
    }

    @Override
    public Case addWorkToCase(Long caseId,Long workId){
        Case case_=loadCase(caseId);
        Set<Long> works=case_.getWorkId();
        if(works.contains(workId))
            return null;
        works.add(workId);
        case_.setWorkId(works);
        return loadCase(caseId);
    }

    @Override
    public Case deleteWorkFromCase(Long caseId,Long workId){
        Case case_=loadCase(caseId);
        Set<Long> works=case_.getWorkId();
        if(!works.contains(workId))
            return null;
        works.remove(workId);
        case_.setWorkId(works);
        return loadCase(caseId);
    }


}
