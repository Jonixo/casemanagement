package com.databoss.furkan.Dao;

import com.databoss.furkan.Classes.Work;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class WorkDAO implements IWorkDAO{

    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public Work loadWork(Long workId) {
        return entityManager.find(Work.class, workId);
    }

    @Override
    public Work createWork(Work work) {
        entityManager.persist(work);
        entityManager.flush();
        return loadWork(work.getWorkId());
    }

    @Override
    public Work updateWork(Work work,Long workId) {
        Work work_up = loadWork(workId);
        work_up.setCase_id(work.getCaseId());
        work_up.setData(work.getData());
        entityManager.flush();
        return loadWork(workId);
    }

    @Override
    public void deleteWork(Long workId) {
        entityManager.remove(loadWork(workId));
        entityManager.flush();
    }

}
