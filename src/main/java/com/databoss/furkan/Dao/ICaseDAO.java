package com.databoss.furkan.Dao;

import java.util.List;

import com.databoss.furkan.Classes.Case;

public interface ICaseDAO {

        List<Case> getAllCases();

        Case loadCase(Long caseId);

        Case createCase(Case case_);

        Case updateCase(Case case_,Long caseId);

        void deleteCase(Long caseId);

        Case shareCase(Long caseId, String userName, int permissionLevel);

        Case addWorkToCase(Long caseId,Long workId);

        Case deleteWorkFromCase(Long caseId,Long workId);

}
