package com.databoss.furkan.Dao;

import com.databoss.furkan.Classes.Work;


public interface IWorkDAO {

    Work loadWork(Long workId);

    Work createWork(Work work);

    Work updateWork(Work work,Long workId);

    void deleteWork(Long workId);

}
